# Causality - fundamental principles and tools

In this repository, we will provide additional material to perform causal data analysis and further illustrate methods presented in the Book Chapter **Causality: fundamental principles and tools**, *I. Balelli, S. Al Ali, E. Dumas and J. Abecassis*. 

## Causal discovery
In the **Causal_discovery** repository you will find notebooks for generate synthetic data, and run several classical causal discovery algorithms. The causal discovery algorithms are based on the python package `causal-learn`. 

The following packages must be installed in order to run the tutorial for causal discovery:

    python 3 (>=3.7)
    numpy
    pandas
    scipy
    scikit-learn
    statsmodels
    causal-learn (https://github.com/py-why/causal-learn/tree/main)
    networkx
    pydot
    io
